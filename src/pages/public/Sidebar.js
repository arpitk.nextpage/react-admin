import React from 'react'

function Sidebar() {
  return (
<>
<nav>
<div className="sidebar">
<div>
    <h3 className='heading'>Dashboard</h3>
</div>
<div className='menu-bar'>
    <div className='menu'>
    </div>
    <ul className='list'>
        <li>Home</li>
        <li>Product</li>
        <li>Linesheet</li>
        <li>Custom List</li>
        <li>Orders</li>
    </ul>
</div>
</div>
</nav>
<section>
<div className='nav-section'>
<div class="left-space">
<input type="text" placeholder=' Search...' className='search'/>
</div>
</div>
</section>
</>  )
}

export default Sidebar