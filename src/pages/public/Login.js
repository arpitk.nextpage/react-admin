import React,{useState} from 'react'
import {FcGoogle} from 'react-icons/fc'
import {BsFacebook,BsApple} from 'react-icons/bs'
import { useNavigate} from 'react-router-dom'
import { setLocal } from '../../common/Util'

const Login =() =>{
    const navigate = useNavigate();
    const [registration, setRegistration]=useState({
        email:"",
        password:""
    })
   const handleInput=(e)=>{
       const {name, value} =e.target;
       setRegistration((prevState)=>{
        return {
            ...prevState,
            [name]: value,
        }
       });
   } 
    const submitForm=(e)=>{
            e.preventDefault();
            console.log(registration);
            if(registration.email=== "user@yopmail.com" && registration.password=== "12345678"){
                setLocal("LoginInfo", JSON.stringify(registration));
                navigate("/Sidebar");
            }
            else{
                console.log("error");
            }   
    }
    
  return (
    <>
           <main className="main">
          <div className="container">
            <section className="wrapper">
              <div className="heading">
                <h1 className="text text-large">Sign In</h1>

              </div>
              <form name="signin" className="form" onSubmit={submitForm}>
                <div className="input-control">
                  <label htmlFor="email" className="input-label" hidden>Email Address</label>
                  <input type="email" name="email" id="email" className="input-field" placeholder="Email Address" value={registration.email} onChange={handleInput}/>
                </div>
                <div className="input-control">
                  <label htmlFor="password" className="input-label" hidden>Password</label>
                  <input type="password" name="password" id="password" className="input-field" placeholder="Password" value={registration.password} onChange={handleInput}/>
                </div>
                <div className="input-control">
                  <a href="#" className="text text-links">Forgot Password</a>   
                  <input type="submit" name="submit" className="input-submit" defaultValue="Sign In" />
                </div>
              </form>
              <div className="striped">
                <span className="striped-line" />
                <span className="striped-text">Or</span>
                <span className="striped-line" />
              </div>
              <div className="method">
                <div className="method-control">
                  <a href="#" className="method-action">
                    <FcGoogle className='ion ion-logo-google'/>
                    <span>Sign in with Google</span>
                  </a>
                </div>
                <div className="method-control">
                  <a href="#" className="method-action">
                    <BsFacebook className='ion ion-logo-facebook'/>
                    <span>Sign in with Facebook</span>
                  </a>
                </div>
                <div className="method-control">
                  <a href="#" className="method-action">
                    <BsApple className='ion ion-logo-apple' />
                    <span>Sign in with Apple</span>
                  </a>
                </div>
              </div>
            </section>
          </div>
        </main>
    </>
  )
}

export default Login