import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {HashRouter,Routes,Route, Navigate} from 'react-router-dom';
import Sidebar from './pages/public/Sidebar';
import Login from './pages/public/Login';
import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
       <HashRouter>
        <Routes>
          <Route path="/sidebar" element={<Sidebar />} />
          <Route path="/" element={<App />} />
          <Route path="/login" element={<Login />} />
          {/* <Route path='*' element={<Navigate to='/login' replace />} /> */}
        </Routes>
      </HashRouter>

  </React.StrictMode>
);


