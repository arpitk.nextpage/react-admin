export const setLocal = (key, data) => {
    if (typeof key !== "string")
        throw new Error(`Type of key cannot be ${typeof key}`);

    if (typeof data !== "string")
        throw new Error(`Type of data cannot be ${typeof data}`);

    localStorage.setItem(key, data);
};

export const readLocal = (key) => {
    if (typeof key !== "string")
        throw new Error(`Type of key cannot be ${typeof key}`);

    return localStorage.getItem(key);
};